This program utilizes the flask module to create a simple webpage, assuming
a request is made for an .html file that exists in the "templates" directory.
Otherwise, an appropriate error message will be returned (either 403 or 404). 

Name: Alejandro De Paz
Email: adepaz@uoregon.edu
