from flask import Flask,render_template, request
import os

app = Flask(__name__)

@app.route("/")
def index():
    return "Nothing to see here"

@app.route("/<name>")
def main(name):
    
    try:
        return render_template(name)
    except:

        if ("//" in name) or (".." in name) or ("~" in name):
            return error_403(403)

        else:
            if os.path.isfile("./template/" + name) == False:
                return error_404(404)

@app.errorhandler(404)
def error_404(error):
    return render_template("404.html"), 404

@app.errorhandler(403)
def error_403(error):
    return render_template("403.html"), 403

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
